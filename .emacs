(require 'package)
(setq
 package-archives '(("gnu" . "http://elpa.gnu.org/packages/")
                    ("org" . "http://orgmode.org/elpa/")
                    ("melpa" . "http://melpa.org/packages/")
                    ("melpa-stable" . "http://stable.melpa.org/packages/")))
(package-initialize)

(tool-bar-mode -1)

(when (not package-archive-contents)
  (package-refresh-contents))

(require 'use-package)

(setq use-package-verbose t)

(use-package helm
  :ensure t
  :init
  (setq helm-mode-fuzzy-match t)
  (setq helm-completion-in-region-fuzzy-match)
  :bind (("C-x C-b" . helm-buffers-list)
	 ("M-x" . helm-M-x)
	 ("C-x b" . helm-mini))
  :config
  (helm-mode 1)
  (helm-autoresize-mode 1))

(use-package highlight-symbol
  :diminish highlight-symbol-mode
  :commands highlight-symbol
  :bind ("s-h" . highlight-symbol))

(use-package popup-imenu
  :commands popup-imenu
  :bind ("M-i" . popup-imenu))

(use-package magit
  :commands magit-status magit-blame
  :init (setq
         magit-revert-buffers nil)
  :bind (("s-g" . magit-status)
         ("s-b" . magit-blame)))

(use-package company
  :diminish company-mode
  :commands company-mode
  :init
  (setq
   company-dabbrev-ignore-case nil
   company-dabbrev-code-ignore-case nil
   company-dabbrev-downcase nil
   company-idle-delay 0
   company-minimum-prefix-length 4)
  :config
  ;; disables TAB in company-mode, freeing it for yasnippet
  (define-key company-active-map [tab] nil))

(use-package ensime
  :pin melpa-stable
  :config
  (add-hook 'scala-mode-hook 'ensime-scala-mode-hook)
  (add-hook 'scala-mode-hook (lambda () (setq prettify-symbols-alist (append scala-prettify-symbols-alist prettify-symbols-alist)))))


(use-package smartparens-config
  :config
  (smartparens-global-mode t)
  (add-hook 'smartparens-enabled-hook #'evil-smartparens-mode))

(use-package powerline)
(use-package solarized-dark-theme)
(use-package emmet-mode)
(use-package linum
  :config
  (global-linum-mode 1))

(use-package rainbow-delimiters)

(use-package proof-site
  :load-path "/usr/share/emacs/site-lisp")

(use-package company-coq
  :config (add-hook 'coq-mode-hook #'company-coq-mode))

(use-package company-auctex
  :config
  (company-auctex-init)
  (set-default 'preview-scale-function 1.5))

(use-package projectile
  :init (setq rojectile-use-git-grep t)
  :config (projectile-global-mode t))

(use-package yasnippet
  :diminish yas-minor-mode
  :commands yas-minor-mode
  :config (yas-reload-all)
  (yas-global-mode 1))

(setq prettify-symbols-unprettify-at-point t)

(set-face-attribute 'default t :font "Source Code Pro-12" )
(set-default-font "Source Code Pro-12")

(custom-set-variables
 ;; custom-set-variables was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(custom-enabled-themes (quote (misterioso)))
 '(custom-safe-themes
   (quote
    ("8db4b03b9ae654d4a57804286eb3e332725c84d7cdab38463cb6b97d5762ad26" default)))
 '(initial-buffer-choice t)
 '(pdf-latex-command "xelatex"))
(custom-set-faces
 ;; custom-set-faces was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 )
